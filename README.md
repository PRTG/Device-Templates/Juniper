PRTG Device Template for Juniper / JunOS Switches
===========================================

Please document and rename this file to "README.md"

This project contains all the files necessary to integrate the Juniper switch template
into PRTG for auto discovery and sensor creation.

Download Instructions
=========================
 [A zip file containing all the files in the project can be downloaded from the 
repository](https://gitlab.com/PRTG/Device-Templates/Juniper/-/jobs/artifacts/master/download?job=PRTGDistZip) 
.

Installation Instructions
=========================
Please refer to the original article ["How can I monitor the health of Juniper devices?"](https://kb.paessler.com/en/topic/72738-how-can-i-monitor-the-health-of-juniper-devices) in the Paessler KB Articles.

